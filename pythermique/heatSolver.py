
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import pickle

from scipy.integrate import solve_ivp
from numba import njit


@njit()
def interp_3Dtab_over_time(tab, ts, ti):
    if ti == 0.:
        return tab[:, :, 0]
    if ti == ts[-1]:
        return tab[:, :, -1]
    id_0 = np.where(ti > ts)[0][-1]
    id_1 = np.where(ti < ts)[0][0]
    t_0 = ts[id_0]
    t_1 = ts[id_1]
    frac_t_i = (ti - t_0)/(t_1 - t_0)

    sel = tab[:, :, id_0:id_1+1]
    res = frac_t_i * sel[:, :, 0] + (1-frac_t_i) * sel[:, :, 1]
    return res
@njit()
def interp_Stab_over_time(tab, ts, ti):
    if ti == 0.:
        res= tab[..., 0]
    if ti == ts[-1]:
        res=  tab[..., -1]
    id_0 = np.where(ti >= ts)[0][-1]
    id_1 = id_0 + 1
    t_0 = ts[id_0]
    t_1 = ts[id_1]
    frac_t_i = (ti - t_0)/(t_1 - t_0)

    sel = tab[..., id_0:id_1+1]
    res = (1-frac_t_i) * sel[..., 0] + (frac_t_i) * sel[..., 1]
    return res

@njit()
def Tp_def(t, Tin, NX, NY, dx, dy, rhoCp, k, S_tab, ts):
    """
    compute DT/dt for the 2D heat eq
    """
    Tp = np.zeros((NX, NY))
    T = Tin.copy().reshape((NX, NY))
    # laplacien estimaion usind finite difference method
    for i in range(1, NX-1):
        for j in range(1, NY-1):
            Tp[i, j] = (T[i-1, j] - 2. * T[i, j] + T[i+1, j]) /(dx**2) \
                 + (T[i, j-1] - 2. * T[i, j] + T[i, j+1])/(dy**2)

    Tp *= k

    # Heat loos by convetive echanges trought side of the sample
    e = 1.55e-3
    hl = 10
    Tp -= 2*hl / e * T 
    
    # Source
    S = interp_3Dtab_over_time(S_tab, ts, t)
    Tp += S 

    # CL de Neumann
    # Null flux
    if 1:
        Tp[0, :] = Tp[1, :]  # left
        Tp[-1, :] = Tp[-2, :]  # right
    # phi = - h DT
    

    # Flux
    if 1:
        Text = 0.
        # Bot    
        hm = 300.
        C = - hm * (Text - T[:, 0])
        Tp[:, 0] = k /dy**2 * (2*T[:, 1] -2*T[:, 0] - 2 * dy * C)
        # Top
        C = - hm * (Text - T[:, -1])
        Tp[:, -1] = k/dy**2 * (2*T[:, -2] - 2*T[:, -1] - 2 * dy * C) 

    # divid by rho*Cp
    Tp /=  rhoCp
    return Tp.reshape(-1)

def Tp_def_1D(t, Tin, NX, dx, rhoCp, k, S_tab, ts, hb):
    """
    compute DT/dt for the 1D heat eq
    """
    Tp = np.zeros((NX))
    T = Tin.copy()
    # laplacien estimaion usind finite difference method
    for i in range(1, NX-1):
        Tp[i] = (T[i-1] - 2. * T[i] + T[i+1]) /(dx**2) 

    Tp *= k

    # Heat loos by convetive echanges trought side of the sample hb = 2*hl / e    
    Tp -= hb * T # heat loss by convection on the side of the sample
    
    # Source
    if 1:
        S = interp_Stab_over_time(S_tab, ts, t)
        Tp += S 

    # CL :
    

    # divid by rho*Cp
    Tp /=  rhoCp
    return Tp.reshape(-1)
class heatSolver1D():
    def __init__(self,  xv, k=0., rho=0., Cp=0., hb=0.):
        self.k = k
        self.rhoCp = rho*Cp
        self.hb = hb

        # NUMERICAL PARAMETERS
        self.NX = xv.shape[0]
        self.dx = np.diff(xv)[0]  # Grid step in x (space)

    def set_source(self, S_tab, ts):
        self.S_tab = S_tab
        self.ts = ts

    def solve(self, t_eval, T_init=0.):
        print("Solve 1D heat equation")
        Duration = t_eval[-1]
        sol = solve_ivp(Tp_def_1D, [0, Duration],
                        T_init,
                        t_eval=t_eval,
                        method='BDF',
                        vectorized=False,
                        args=(self.NX,
                              self.dx, 
                              self.rhoCp,self.k,
                              self.S_tab, self.ts,
                              self.hb))
        T_tab = sol.y.reshape((self.NX, -1))
        return T_tab

class heatSolver():
    def __init__(self,  xv, yv, k=0., rho=0., Cp=0., hb=0.):
        self.k = k
        self.rhoCp = rho*Cp
        self.hb = hb

        # NUMERICAL PARAMETERS
        self.NX, self.NY = xv.shape
        self.dx = np.diff(xv, axis=0)[0, 0]  # Grid step in x (space)
        self.dy = np.diff(yv, axis=1)[0, 0]  # Grid step in y (space)

    def set_source(self, S_tab, ts):
        self.S_tab = S_tab
        self.ts = ts

    def solve(self, t_eval, T_init=0.):
        Duration = t_eval[-1]
        sol = solve_ivp(Tp_def, [0, Duration],
                        T_init,
                        t_eval=t_eval,
                        method='BDF',
                        vectorized=False,
                        args=(self.NX, self.NY,
                              self.dx, self.dy, 
                              self.rhoCp,self.k,
                              self.S_tab, self.ts,
                              self.hb))
        T_tab = sol.y.reshape((self.NX, self.NY, -1))
        return T_tab


