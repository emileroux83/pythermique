__version__ = "0.0"

try:
    from . import utils, mesh, camRegistration, mask2Mesh, heatSolver, study
except:
    print("Failed ot import pythermique")
