# -*- coding: utf-8 -*-
"""
Created on 09/03/2023

@author: rouxemi
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage
from skimage.feature import match_template

def cart2pol(X):
    x=X[:,0]-X[:,0].mean()
    y=X[:,1]-X[:,1].mean()
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def CornerDetect(image,w=15, plot=False):  
    template = np.zeros((2*w,2*w))
    template[:w,w:]=1
    template[w:,:w]=1
    template = ndimage.gaussian_filter(template, sigma=.2*w)
    
    result = np.abs(match_template(image, template, pad_input=True))
    corners=[]
    result_t=result.copy()
    for i in range(4):
        ij = np.unravel_index(np.argmax(result_t), result_t.shape)
        x, y = ij[::-1]
        result_t[y-w:y+w, x-w:x+w] = 0.
        corners.append(np.array([x,y]))
    
    corners=np.asarray(corners)
    c = corners.copy()
    c=c-c.mean()
    rho,phi = cart2pol(c)
    print(np.rad2deg(phi))
    swap_id = np.argsort(phi)
    print(np.rad2deg(phi[swap_id]))
    corners=corners[swap_id,:]
    
    if plot:
        plt.figure()
        plt.imshow(template, cmap=plt.cm.gray)
        
        plt.figure()
        plt.imshow(image, cmap=plt.cm.gray)  
        for c in corners:
            plt.plot(c[0],c[1],'o')   
        #for c in coords_subpix:
            #plt.plot(c[0],c[1],'r+')     
        
        plt.figure()
        plt.imshow(image, cmap=plt.cm.gray)
        res = plt.imshow(result, alpha=0.2)
        for c in corners:
            plt.plot(c[0],c[1],'o')
        plt.colorbar(res)
    
    
    return corners





def Corners_TH_VIS_Detection(image_th, image_vis):
    corners_th = CornerDetect(image_th,w=7, plot=True)
    corners_vis = CornerDetect(image_vis,w=51, plot=True)
    plt.show()


    fig, axs = plt.subplots(1, 2)
    axs[0].imshow(image_vis)
    axs[1].imshow(image_th)
    plt.title('OK ?')
    col = 'rgby'
    for i in range(4):
        axs[0].plot(corners_vis[i,0], corners_vis[i,1], col[i]+'o')
        axs[1].plot(corners_th[i,0] , corners_th[i,1] , col[i]+'o')
    plt.show()    
    plt.close()

    return corners_th, corners_vis

if __name__ == "__main__":
    # path to images
    path = r"D:/Utilisateurs/rouxemi/Documents/Documents/Recherche/Flir/Test/22_01_01_IR_VIS_4/"
    image_th =plt.imread(path+'ref/ref_th.tif')
    image_th = image_th.sum(axis=2)/3.
    image_vis =plt.imread(path+'ref/ref_vis.tif')

    corners_th, corners_vis = Corners_TH_VIS_Detection(image_th, image_vis)

    np.save('corners_th.npy', corners_th )
    np.save('corners_vis.npy', corners_vis )

