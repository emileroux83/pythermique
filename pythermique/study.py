# -*- coding: utf-8 -*-
"""
Created on Mon Oct 2023

@author: rouxemi
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import pickle 
"""
class Study():
    def __init__(self, meta='',path=None, reopen=True) -> None:
        self.path = path
        if reopen:
            with open("{}.pkl".format(path), 'rb') as f:
                self.grid_data = pickle.load(f)
            self.meta = pd.read_csv(path+'meta.csv', index_col='id')
        else:
            self.grid_data={}
            self.meta = meta
        
    def __getitem__(self, item):
        if item in self.grid_data:
            return self.grid_data[item]
        else:
            print('Warnning : Item {} note define in the grid data.'.format(item))
    
    def __repr__(self) -> str:
        rep = "< Study : {} >".format(self.path)
        for k,v in self.grid_data.items():
            rep += "\n  {} : shape={}".format(k,v.shape)
        rep += "\n>"
        return rep


    def set_grid_data(self,key, data):
        self.grid_data[key] = data
    
    def save(self,save_path):
        with open("{}.pkl".format(save_path), 'wb') as f:
             pickle.dump(self.grid_data, f)
        self.meta.to_csv(save_path+'meta.csv')
"""  
## exemple d'affichage
import ipywidgets as widgets

class Study():
    def __init__(self, meta='',path=None, reopen=True) -> None:
        self.path = path
        if reopen:
            with open("{}.pkl".format(path), 'rb') as f:
                self.grid_data = pickle.load(f)
            self.meta = pd.read_csv(path+'meta.csv', index_col='id')
        else:
            self.grid_data={}
            self.meta = meta
        
    def __getitem__(self, item):
        if item in self.grid_data:
            return self.grid_data[item]
        else:
            print('Warnning : Item {} note define in the grid data.'.format(item))
    
    def __repr__(self) -> str:
        rep = "< Study : {} >".format(self.path)
        for k,v in self.grid_data.items():
            rep += "\n  {} : shape={}".format(k,v.shape)
        rep += "\n>"
        return rep


    def set_grid_data(self,key, data):
        self.grid_data[key] = data
    
    def save(self,save_path):
        with open("{}.pkl".format(save_path), 'wb') as f:
             pickle.dump(self.grid_data, f)
        self.meta.to_csv(save_path+'meta.csv')
        
    def plot_field(self, field_name, frame=0):
        field = self.grid_data[field_name]
        valT = np.nanmax(np.abs(field))
        levels = np.linspace(-1, 1, 21)
        fig = plt.figure()
        axs = fig.subplots(1, 2)

        self.p0 = [axs[0].contourf(self["xv_d"], self["yv_d"], field[...,frame], cmap=mpl.cm.coolwarm, levels=levels*valT)]
        axs[0].set_aspect("equal")
        axs[0].set_title(field_name)
        cbar = fig.colorbar(self.p0[0], ax=axs[0])
        for ax in axs:
            ax.axis('off')
        self.axs = axs

    def interact_plot(self, field_name):
        self.plot_field(field_name)
        field = self.grid_data[field_name]
        @widgets.interact(frame=(0, field.shape[2]-1, 1))
        def plot_T(frame=0):
            field = self.grid_data[field_name][:, :, frame]
            valT = np.nanmax(np.abs(self.grid_data[field_name]))
            levels = np.linspace(-1, 1, 21)

            for tp in self.p0[0].collections:
                tp.remove()    

            self.p0[0] = self.axs[0].contourf(self["xv_d"], self["yv_d"], field, cmap=mpl.cm.coolwarm, levels=levels*valT)
            
