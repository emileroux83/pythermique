# -*- coding: utf-8 -*-
"""
Created on Mon Sep  6 14:51:40 2021

@author: rouxemi
"""

from scipy.spatial.distance import pdist, squareform
from matplotlib.widgets import Slider
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import glob
from numba import njit


class silder_plot():

    def __init__(self, T, S, meta):
        self.T = T
        self.S = S
        self.meta = meta
        # Create the figure and the line that we will manipulate
        self.fig, ax = plt.subplots(1, 3)
        self.surfT = ax[0].imshow(T[0, :, :], cmap=mpl.cm.bwr)
        self.surfS = ax[1].imshow(S[0, :, :], cmap=mpl.cm.bwr)
        ax[2].plot(meta.Force_N)
        self.pltF, = ax[2].plot(0, meta.Force_N[0], 'or')
        plt.colorbar(self.surfT, ax=ax[0])
        plt.colorbar(self.surfS, ax=ax[1])

        # Make a horizontal slider to control the frequency.
        ax[0] = plt.axes([0.1, 0.1, 0.65, 0.02])
        self.id_slider = Slider(
            ax=ax[0],
            label='id',
            valmin=0,
            valmax=len(self.T)-1,
            valinit=0,
            valstep=1
        )
        # register the update function with each slider
        self.id_slider.on_changed(self.update)
        plt.show()

    # The function to be called anytime a slider's value changes
    def update(self, val):
        dataT = self.T[int(self.id_slider.val), :, :]
        dataS = self.S[int(self.id_slider.val), :, :]

        self.surfT.set_array(dataT)
        self.surfT.set_norm(None)

        self.surfT.set_clim([np.nanmin(self.T), np.nanmax(self.T)])
        self.surfT.set_clim([np.nanmin(dataT), np.nanmax(dataT)])
        b = np.max(np.abs([np.nanmin(dataT), np.nanmax(dataT)]))
        self.surfT.set_clim([-b, b])

        self.surfS.set_array(dataS)
        self.surfS.set_norm(None)
        self.surfS.set_clim([np.nanmin(self.S), np.nanmax(self.S)])
        self.surfS.set_clim([np.nanmin(dataS), np.nanmax(dataS)])
        b = np.max(np.abs([np.nanmin(dataS), np.nanmax(dataS)]))
        self.surfS.set_clim([-b, b])

        self.pltF.set_xdata([val])
        self.pltF.set_ydata(self.meta.Force_N[val])
        self.fig.tight_layout()
        self.fig.canvas.draw_idle()


def read_data(path_meta='', path_vis='', path_th='', i_max=100000):

    meta = pd.read_csv(path_meta+'Trac.csv', skiprows=13)
    meta['Time'] = pd.to_timedelta(meta['t_s'])

    vis_files = glob.glob(path_vis+'/*.tif')
    th_files = glob.glob(path_th+'/*.npy')

    meta['vis_imgs'] = vis_files

    th_images = {}
    for i in range(min(i_max,len(th_files))):
        image_th = np.load(th_files[i])
        th_images[i] = image_th

    vis_images = {}
    for i in range(min(i_max,len(th_files))):
        image_vis = plt.imread(vis_files[i])
        vis_images[i] = image_vis

    meta = meta[meta.index<i_max]

    return meta, vis_images, th_images

# =============================================================================
# muDIC wrapper
# =============================================================================
def extensometre_from_images(path_vis='', Lx=100., Ly=200., Cx=0, Cy=0.):
    import muDIC as dic

    image_stack = dic.image_stack_from_folder(path_vis, file_type=".tif")
    mesher = dic.Mesher()
    mesh = mesher.mesh(image_stack, Xc1=Cx+Lx/2-30, Xc2=Cx+Lx/2+30,
                       Yc1=Cy, Yc2=Cy+Ly, n_elx=2, n_ely=int(Ly/30), GUI=False)
    inputs = dic.DICInput(mesh, image_stack)

    dic_job = dic.DICAnalysis(inputs)
    results = dic_job.run()

    fields = dic.Fields(results)
    U = fields.disp()
    L0y = fields.coords()[0, 1, 0, -1, 0] - U[0, 1, 0, 1, 0]
    DLy = U[0, 1, 0, -1, :] - U[0, 1, 0, 1, :]
    epsyy = np.log(1+DLy/L0y)

    return epsyy


def strain_from_images(path_vis='', Lx=100., Ly=200., Cx=0, Cy=0.):
    import muDIC as dic

    image_stack = dic.image_stack_from_folder(path_vis, file_type=".tif")
    mesher = dic.Mesher()
    mesh = mesher.mesh(image_stack, Xc1=Cx, Xc2=Cx+Lx,
                       Yc1=Cy, Yc2=Cy+Ly, n_elx=int(Ly/50), n_ely=int(Ly/50), GUI=False)
    inputs = dic.DICInput(mesh, image_stack)

    dic_job = dic.DICAnalysis(inputs)
    results = dic_job.run()

    fields = dic.Fields(results)
    true_strain = fields.true_strain()

    return true_strain

# =============================================================================
# Smoothing using Wavelet
# =============================================================================
import pywt
def wavelet_filtering_3D(T, levels = 11, cut=[9,7,7], wl='db4'):
    cut = np.asarray(cut)
    fs_result = pywt.fswavedecn(T,wl, mode='constant', levels=levels)
    c = fs_result.coeff_slices
    for k in fs_result.detail_keys():
        #print(k, ':', (np.asarray(k)>6).any())
        if (np.asarray(k)>=cut).any():
            # mult = (k-cut)
            # mult[np.where(mult<0)]=0
            # mult=mult.sum()
            # fs_result[k] = fs_result[k]/(2**mult)
            fs_result[k] = np.zeros_like(fs_result[k])
    
    T_w = pywt.fswaverecn(fs_result)
    return T_w, fs_result

# =============================================================================
# Smoothing data usinf fft10
# =============================================================================
def Tmatrix_from_mesh(Bimesh, nx, ny):
    mf = Bimesh.mesh.fields_metadata()
    mf = mf[(mf.label=='T')]
    Ts = []
    for i in range(len(mf)):
        idx = mf[(mf.frame==i)].index
        T = Bimesh.mesh.fields[i].data.values       
        Ts.append(T.reshape(ny+1, nx+1))
    T = np.asarray(Ts)
    return T


def LowPass3D(T, dts = (1.,1.,1.), fcs=(0.5,0.5,0.5), order=4., npad=0):
    """
    axe 0 : X
    axe 1 : Y
    axe 2 : t
    """
    Tmean = T.mean()
    T -=Tmean 
    if npad>0:
        T = np.pad(T,npad, 'reflect')   
    nx,ny,nt = T.shape        

    
    dx,dy,dt = dts
    freq_x = np.fft.fftfreq(nx, d=dx)
    freq_y = np.fft.fftfreq(ny, d=dy)
    freq_t = np.fft.fftfreq(nt, d=dt)
        
    freq_x, freq_y, freq_t = np.meshgrid(freq_y,freq_x,freq_t)
    fx = np.fft.fftshift(freq_x)
    fy = np.fft.fftshift(freq_y)
    ft = np.fft.fftshift(freq_t)
    
    fxc,fyc,ftc = fcs

    D = np.sqrt((fx/fxc)**2 + (fy/fyc)**2 + (ft/ftc)**2)
    F = 1./np.sqrt(1+D**(2.*order))

    T_fft = np.fft.fftn(T)
    T_fft = np.fft.fftshift(T_fft)
    T_fft = F * T_fft
    T_fft = np.fft.ifftshift(T_fft)
    T_ifft = np.fft.ifftn(T_fft)
    T_res = np.real(T_ifft)
    
    if npad>0:
        T_res = T_res[npad:-npad,npad:-npad, npad:-npad]
    
    return T_res+Tmean, F, D, freq_x, freq_y, freq_t


# =============================================================================
# Smooting using Weight Local Linear Regression
# =============================================================================

def model_space_time(X):
    nb_measure = X.shape[0]
    dim = 6 
    M = np.zeros((nb_measure, dim))
    for j in range(nb_measure):
        M[j,0] = 1.
        M[j,1] = X[j,0]
        M[j,2] = X[j,1]
        M[j,3] = X[j,0]**2
        M[j,4] = X[j,1]**2
        M[j,5] = X[j,2]
        
    return M

def model(X):
    nb_measure = X.shape[0]
    dim = 6 
    M = np.zeros((nb_measure, dim))
    for j in range(nb_measure):
        M[j,0] = 1.
        M[j,1] = X[j,0]
        M[j,2] = X[j,1]
        M[j,3] = X[j,0]**2
        M[j,4] = X[j,1]**2
        M[j,5] = X[j,0]*X[j,1]

        
    return M

def predict(X, y, W,  point):
    W=np.atleast_2d(W)
    X_ = model(X)
    
    # Construcion of the Y matrix
    Y = np.atleast_2d(y).T
    # point is the x where we want to make the prediction.
    point_ = np.atleast_2d(point)
    point_ = model(point_)
    XtW = np.dot(X_.T, W)
    beta = np.dot(np.linalg.inv(np.dot(XtW, X_)), np.dot(XtW, Y))

    # Calculating predictions.
    pred = np.dot(point_, beta)

    # Returning the theta and predictions
    return beta, pred


def get_weight_matrix_pdist(X, metric):
    dist_array = pdist(X/metric)
    dist_matrix = squareform(dist_array)
    sig = 1.
    w_matrix = np.exp(-dist_matrix**2)
    return w_matrix

def get_weight_matrix(dist):
    w_matrix = np.exp(-dist**2)
    return w_matrix

def plot_reg(beta):
    x = np.linspace(np.min(X), np.max(X), 21)
    X_ = model(x)
    return x, np.dot(X_, beta)


def smooth_data(X, y, metric, kept_weight= 0.5):
    w_matrix = get_weight_matrix_pdist(X, metric)
    y_smooth = np.zeros(len(X))
    for i in range(len(X)):
        point = X[i, :]
        w = w_matrix[i, :]
        id_, = np.where(w > kept_weight)
        W = w_matrix[i, id_] * np.eye(len(id_))
        beta, p = predict(X[id_, :], y[id_], W, point)
        y_smooth[i] = p
        if i%100 == 0 : print(i,'/',len(X), ', Nb points:',len(id_) )
    return y_smooth
