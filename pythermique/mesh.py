# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 15:29:12 2021

@author: rouxemi
"""

from skimage import transform
from scipy.interpolate import RectBivariateSpline
import numpy as np
import argiope as ag
import pandas as pd
from tps import ThinPlateSpline

def get_field(mesh,label=None, frame=None, step_num=-1,  copy=True, position = 'node', fields_metadata = None):
    "return (a copy or note) of a fields for a given label and a given frame"
    if fields_metadata is None:
        meta_fields = mesh.fields_metadata()
    else:
        meta_fields = fields_metadata
    field_id = meta_fields[(meta_fields.label==label) & (meta_fields.frame==frame) & (meta_fields.position==position) & (meta_fields.step_num==step_num)].index[0]
    if copy:
        return mesh.fields[field_id].copy()
    else: 
        return mesh.fields[field_id]

class BiMesh(ag.utils.Container):
    def __init__(self, mesh,  corner_0, corner_1, corner_metric):
        # Transformation
        self.tform01 = transform.ProjectiveTransform()
        self.tform01.estimate(corner_0, corner_1)
        self.tform10 = transform.ProjectiveTransform()
        self.tform10.estimate(corner_1, corner_0)
        self.tform0metric = transform.ProjectiveTransform()
        self.tform0metric.estimate(corner_0, corner_metric)
        self.mesh = mesh
        self.state = 0

    def __repr__(self):
        return "<Mesh, curent state {0}>".format(self.state)

    def get_field(self,label=None, frame=None, copy=True, position = 'node',step_num= -1, fields_metadata = None):
        return get_field(self.mesh,label=label, frame=frame, step_num= step_num, copy=copy, position = position, fields_metadata = fields_metadata)
    
    
    def _transfomr_mesh(self, trans):
        # Coord transform
        curent_coord = self.mesh.nodes.coords[['x', 'y']]
        modify_coord = trans(curent_coord)
        self.mesh.nodes.loc[:, ('coords', ['x', 'y'])] = modify_coord

    def project_Ufield_to_metric(self):
        """
        in DIC config (0), function that creats new  U fields in [m]
        """
        # Set DIC State
        self.switch_state(0)
        trans=self.tform0metric
        # Find frame to work on it
        frames_to_switch = np.unique(self.mesh.fields_metadata().frame)
        # curent coords
        DIC_coord = self.mesh.nodes.coords[['x', 'y']]
        # coords in metric state
        metric_coord = trans(DIC_coord)
        meta_fields = self.mesh.fields_metadata()
        for f in frames_to_switch:
            
            F = self.get_field(label='U', frame=f, copy=True, fields_metadata=meta_fields)
            F_meta = F.metadata().to_dict()
            # in state DIC : curent coords + U => tip of U vectors
            U_tip = DIC_coord.values + F.data.values

            # (Switch tip of U vectors in metric state) - coord in metric
            metric_U = trans(U_tip) - metric_coord
            # save field to mesh
            df = pd.DataFrame(metric_U)
            F_meta['label'] = 'U[m]'
            field = ag.mesh.Vector2Field(data=df, **F_meta)
            self.mesh.add_fields([field])


    def switch_state(self, to_state=0, talk = False):
        def print_info(state,to_state):
            if talk:
                print('Switch performed {0}->{1}'.format(state,to_state))

        if (self.state == 0) & (to_state == 1) : # trans 0->1
            print_info(self.state,to_state)
            self._transfomr_mesh(self.tform01)
            self.state = 1

        elif (self.state == 1) & (to_state == 0): # trans 1->0
            print_info(self.state,to_state)
            self._transfomr_mesh(self.tform10)
            self.state = 0


        elif (self.state == 0) & (to_state == 2): # trans 0->metric
            print_info(self.state,to_state)
            self._transfomr_mesh(self.tform0metric)
            self.state = 2


        elif (self.state == 2) & (to_state == 0): # trans metric->0
            print_info(self.state,to_state)
            self._transfomr_mesh(self.tform0metric.inverse)
            self.state = 0


        elif (self.state == 1) & (to_state == 2): # trans 1->metric
            print_info(self.state,to_state)
            self._transfomr_mesh(self.tform10)
            self._transfomr_mesh(self.tform0metric)
            self.state = 2

        elif (self.state == 2) & (to_state == 1): # trans metric->1
            print_info(self.state,to_state)
            self._transfomr_mesh(self.tform0metric.inverse)
            self._transfomr_mesh(self.tform01)            
            self.state = 1
        else:
            print_info(self.state,to_state)
            
    def add_field_from_image(self, img, **kwargs):
        spline = RectBivariateSpline(np.arange(img.shape[0]),
                                     np.arange(img.shape[1]),
                                     img)
        coords = self.mesh.nodes.coords[['x', 'y']].values
        data = spline(coords[:, 1], coords[:, 0], grid=False)
        df = pd.DataFrame(data)
        df.index += 1
        field = ag.mesh.ScalarField(data=df, **kwargs)
        self.mesh.add_fields([field])
    
    def interp_data_from_image(self, img):
        spline = RectBivariateSpline(np.arange(img.shape[0]),
                                     np.arange(img.shape[1]),
                                     img)
        coords = self.mesh.nodes.coords[['x', 'y']].values
        data = spline(coords[:, 1], coords[:, 0], grid=False)
        df = pd.DataFrame(data)
        return df
        
    def to_triangulation(self, state=0):
        init_state = self.state
        self.switch_state(to_state=state)
        tri = self.mesh.to_triangulation()
        self.switch_state(to_state=init_state)
        
        return tri

    def smmoothing_tps(self,field_name='',frame=0, alpha=.001, extension='_smooth',
                     time_slices =[-1,0,1],
                     time_derivative=False,
                     laplacian = False):

        adim_coord = [0.01, 0.01, 20]

        meta_fields = self.mesh.fields_metadata()
        max_frame = meta_fields.frame.max()
        # remove old field
        self.remove_fields_from_name(field_name+extension, frame)

        # remove negative time silce
        time_slices = np.array(time_slices)
        time_slices = time_slices[(time_slices + frame) >=0]
        time_slices = time_slices[(time_slices + frame) <= max_frame ]

        # mesh distortion 
        disp = self.get_field(label='U', frame=frame, fields_metadata=meta_fields).data
        Bimesh_Temp = self.copy()        
        Bimesh_Temp.switch_state(to_state=0) #DIC config
        Bimesh_Temp.mesh.nodes[("coords", "x")] += disp.v1
        Bimesh_Temp.mesh.nodes[("coords", "y")] += disp.v2

        Bimesh_Temp = self.copy()
        Bimesh_Temp.switch_state(0)
        Bimesh_Temp.mesh.nodes[("coords", "x")] += disp.v1
        Bimesh_Temp.mesh.nodes[("coords", "y")] += disp.v2  
  
        # Curent frame 
        F0 = self.get_field(label=field_name, frame=frame)
        meta_F0 = F0.metadata().to_dict()
        Tri = Bimesh_Temp.to_triangulation(state=2)    
        # cx, cy , t   
        t0 = meta_F0['frame_value'] 
        X0 = np.array([Tri.x, Tri.y, t0 * np.ones_like(Tri.y)]).T
        Y0 = F0.data.v
        
        X = X0.copy()
        Y = Y0.copy()

        for i in [0,1,2]:
            X0[:,i] /= adim_coord[i]
        
        for time_slice in time_slices:
            if time_slice != 0 :
                F = self.get_field(label=field_name, frame=frame+time_slice)
                meta_F = F.metadata().to_dict()
                t = meta_F['frame_value']            
                Y = np.hstack((Y,F.data.v))
                Tri = Bimesh_Temp.to_triangulation(state=2)        
                coords = np.array([Tri.x, Tri.y, t * np.ones_like(Tri.y)]).T
                X = np.vstack((X,coords))
        Y = np.atleast_2d(Y).T

        for i in [0,1,2]:
            X[:,i] /= adim_coord[i]
        
        # Create the tps object
        tps = ThinPlateSpline(alpha=alpha)  # 0 Regularization
        # Fit tps 
        tps.fit(X, Y)

        # Smooth data
        T_smooth = tps.transform(X0)[:,0]
        meta_F0["label"] = field_name+extension
        df = pd.DataFrame(T_smooth)
        df.index += 1
        field = ag.mesh.ScalarField(data=df, **meta_F0)
        self.mesh.add_fields([field])
        print(field)

        # Time derivative
        if time_derivative:
            eps = 0.001
            X0dt = X0.copy()
            X0dt[:,2] += eps
            T_dt = (tps.transform(X0dt)[:,0] - T_smooth)/eps
            F0 = self.get_field(label=field_name, frame=frame)
            meta_F0 = F0.metadata().to_dict()
            meta_F0["label"] = field_name+extension+'_dt'
            df = pd.DataFrame(T_dt)
            df.index += 1
            field = ag.mesh.ScalarField(data=df, **meta_F0)
            # remove old field
            self.remove_fields_from_name(field_name+extension+'_dt', frame)
            self.mesh.add_fields([field])        
            print(field)

        # Laplacian computation
        if laplacian:
            eps = 0.001
            # D^2T / dx^2
            X0dxp = X0.copy()
            X0dxp[:,0] += eps
            X0dxm = X0.copy()
            X0dxm[:,0] -= eps
            T_dxx = (tps.transform(X0dxp)[:,0] - 2*T_smooth + tps.transform(X0dxm)[:,0])/eps**2

            # D^2T / dy^2
            X0dyp = X0.copy()
            X0dyp[:,1] += eps
            X0dym = X0.copy()
            X0dym[:,1] -= eps
            T_dyy = (tps.transform(X0dyp)[:,0] - 2*T_smooth + tps.transform(X0dym)[:,0])/eps**2

            lapp = T_dxx + T_dyy

            F0 = self.get_field(label=field_name, frame=frame)
            meta_F0 = F0.metadata().to_dict()
            meta_F0["label"] = field_name+extension+'_lap'
            df = pd.DataFrame(lapp)
            df.index += 1
            field = ag.mesh.ScalarField(data=df, **meta_F0)
            # remove old field
            self.remove_fields_from_name(field_name+extension+'_lap', frame)
            self.mesh.add_fields([field])        
            print(field)

    def remove_fields_from_name(self, name_to_remove, frame_to_remove):
        # Remove fileds
        meta_fields = self.mesh.fields_metadata()
        id_to_pop = meta_fields.index[(meta_fields.label==name_to_remove) & (meta_fields.frame==frame_to_remove)].to_numpy()[::-1]
        id_to_pop
        for i in id_to_pop:
            self.mesh.fields.pop(i)

    def smmoothing_tps_all_frames(self,field_name='', alpha=.001,
                     time_slices =[-1,0,1],
                     time_derivative=False,
                     laplacian = False):
        """
        Smooth the given field using thin plate spline methode
        """
        meta_fields = self.mesh.fields_metadata()
        #loop over frames   
        for i in meta_fields.frame.unique():
            self.smmoothing_tps(field_name= field_name,frame= i,
             alpha= alpha,
             time_slices= time_slices,
             time_derivative= time_derivative,
             laplacian= laplacian )
            

