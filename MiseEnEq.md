# Mise en equation du pb thermoméca

## Modèle Thermique simplifié 2D

### Equation de la chaleur
$$
\rho C_{\varepsilon, \alpha} \frac{\partial \bar{\theta}(\mathbf{x}, t)}{\partial t}+\frac{2 h}{e} \bar{\theta}(\mathbf{x}, t)-k \Delta \bar{\theta}(\mathbf{x}, t)=\bar{s}(\mathbf{x}, t)
$$
ref : An infrared image processing to analyse the calorific
effects accompanying strain localisation
André Chrysochoos, Hervé Louche
,https://hal.science/hal-03349516v1/document

avec 
- $\rho$ : masse volmique
- $C$ :
- $e$ :
- $h$ : 
- $k$ :

reformulation version code

$$
\rho C \frac{\partial T}{\partial t}+\frac{2 h}{e} T-k \Delta T
=
S
$$

reformulation pour resolution par solveur d'equation différentielle
$$
 \frac{\partial T}{\partial t}
 =
 \frac{1}{\rho C} 
 \left(
-\frac{2 h}{e} T
+k \Delta T
+ S
\right)
$$

### Condition au limites
#### Echange par les 2 faces (avant arrière)
Prisen en compte dans l'equation de la chaleur, les pertes sont modélisé comme une pert de chaleur volumique

#### echange latéraux 
hypothèse Flux null

#### echange avec les morhs 
Echange condictif avec un thermostat

$$
\frac{\partial T}{\partial x}(0, t)=\phi
$$

$$
T_{0}^{k+1}=T_{0}^{k}+\frac{\alpha^{2} \Delta t}{\Delta x^{2}}\left(2 T_{1}^{k}-2 T_{0}^{k}-2 \Delta x C\right)
$$
avec $\alpha^2 = \frac{k}{\rho C}$

ref : Math 257: Finite diference methods

reformulation pour resolution par solveur d'equation différentielle
$$
\frac{T_{0}^{k+1}-T_{0}^{k}}{\Delta t}=
\frac{\alpha^{2} }{\Delta x^{2}}\left(2 T_{1}^{k}-2 T_{0}^{k}-2 \Delta x C\right)
$$

$$
 \frac{\partial T}{\partial t}_{x=0}
 =
 \frac{1}{\rho C}
 \frac{k}{\Delta x^{2}}\left(2 T_{1}^{k}-2 T_{0}^{k}-2 \Delta x C\right)
$$

## Estimation de sources thermique
$$
S = 
\rho C \frac{\partial T}{\partial t}+\frac{2 h}{e} T - k \Delta T
$$