from setuptools import setup
import pythermique

setup(name='pythermique',
      version=pythermique.__version__,
      description="pythermique",
      long_description="",
      author='Emile Roux, Ludovic Charleux',
      author_email='emile.roux@univ-smb.fr',
      license='GPL v3',
      packages=['pythermique'],
      zip_safe=False,
      install_requires=[
          "numpy",
          "scipy",
          "matplotlib",
          "pandas",
          ],
      )
